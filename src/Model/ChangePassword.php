<?php

namespace App\Model;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ChangePassword
{
    /**
     * @var string
     */
    public $current;
    
    /**
     * @var string
     */
    public $new;
    
    /**
     * The current logged user
     * @var UserInterface
     */
    public $user;
    
    /**
     * User Password encoder
     * @var UserPasswordEncoderInterface
     */
    public $encoder;
    
    public function __construct(UserInterface $user, UserPasswordEncoderInterface $encoder) 
    {
        $this->user = $user;
        $this->encoder = $encoder;
    }
    
    public function getNewPassword(): string
    {
        return $this->encoder->encodePassword($this->user, $this->new);
    }
}