<?php

namespace App\Model;

use App\Model\Base\ApplicationUser as BaseApplicationUser;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * Skeleton subclass for representing a row from the 'user_app' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ApplicationUser extends BaseApplicationUser implements AdvancedUserInterface, \Serializable
{
    public function eraseCredentials() 
    {
        
    }

    public function getRoles() 
    {
        $roles = parent::getRoles();
        $roles[] = 'ROLE_USER';
        
        return $roles;
    }
    
    public function getSalt() 
    {
        return null;
    }

    public function isAccountNonExpired() 
    {
        return true;
    }

    public function isAccountNonLocked() 
    {
        return true;
    }

    public function isCredentialsNonExpired() 
    {
        return true;
    }

    public function isEnabled() 
    {
        return $this->getIsActive();
    }

    public function serialize() 
    {
        return serialize(array(
            $this->getId(),
            $this->getUsername(),
            $this->getPassword(),
            $this->getEmail(),
            $this->getIsActive()
        ));
    }

    public function unserialize($serialized) 
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->is_active
        ) = unserialize($serialized);
    }
}
