<?php

namespace App\Breadcrumbs;

use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BreadcrumbsBuilder
{
    private $router;
    private $breadcrumbs;
    
    public function __construct(Breadcrumbs $breadcrumbs, UrlGeneratorInterface $router, $homeText = 'Home', $homeRoute = 'homepage') 
    {
        $this->router = $router;
        $this->breadcrumbs = $breadcrumbs;
        $this->breadcrumbs->addItem($homeText, $this->router->generate($homeRoute));
    }
    
    public function add($text, $route, $parameters = array())
    {
        $this->breadcrumbs->addItem($text, $this->router->generate($route, $parameters));
        
        return $this;
    }
}
