<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction(AuthenticationUtils $authHelper): Response
    {
        return $this->render('security/login.html.twig', [
            'error' => $authHelper->getLastAuthenticationError(),
            'last_username' => $authHelper->getLastUsername(),
        ]);
    }
    
    /**
     * @Route("/login_check", name="security_login_check")
     */
    public function loginCheckAction(): void
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }
    
    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout(): void
    {
        throw new \Exception('This should never be reached!');
    }
}
