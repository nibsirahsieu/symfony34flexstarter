<?php

namespace App\Controller;

use App\Breadcrumbs\BreadcrumbsBuilder;
use App\Form\ChangePasswordType;
use App\Model\ChangePassword;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ChangePasswordController extends AbstractController
{
    /**
     * @Route("/change-password", name="change_password")
     */
    public function indexAction(Request $request, UserInterface $user, UserPasswordEncoderInterface $passwordEncoder, BreadcrumbsBuilder $breadcrumbs): Response
    {
        $changePassword = new ChangePassword($user, $passwordEncoder);
        $form = $this->createForm(ChangePasswordType::class, $changePassword);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($changePassword->getNewPassword())->save();
            
            $this->addFlash('success', 'app.success.password_changed');
            
            return $this->redirectToRoute('change_password');
        }
        
        $breadcrumbs->add('Change Password', 'change_password');
        
        return $this->render('change_password/index.html.twig', ['form' => $form->createView()]);
    }
}