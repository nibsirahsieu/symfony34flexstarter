<?php

namespace App\Security\Guard;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;

class FormLoginAuthenticator extends AbstractFormLoginAuthenticator
{
    private $encoder;
    private $urlGenerator;
    private $tokenManager;
    
    private $unauthorized = 'app.unauthorized';
    private $invalidToken = 'app.invalid_token';
    
    public function __construct(UserPasswordEncoderInterface $encoder, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $tokenManager) 
    {
        $this->encoder = $encoder;
        $this->urlGenerator = $urlGenerator;
        $this->tokenManager = $tokenManager;
    }
    
    public function checkCredentials($credentials, UserInterface $user) 
    {
        if (!$this->tokenManager->isTokenValid(new CsrfToken('authenticate', $credentials['csrfToken']))) {
            throw new CustomUserMessageAuthenticationException($this->invalidToken);
        }
        
        if (!$this->encoder->isPasswordValid($user, $credentials['password'])) {
            throw new CustomUserMessageAuthenticationException($this->unauthorized);
        }
        
        return true;
    }

    public function getCredentials(Request $request) 
    {
        $username = $request->request->get('_username');
        $password = $request->request->get('_password');
        $csrfToken = $request->request->get('_csrf_token');
        
        $request->getSession()->set(Security::LAST_USERNAME, $username);
        
        return [
            'username' => $username,
            'password' => $password,
            'csrfToken' => $csrfToken
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider) 
    {
        try {
            return $userProvider->loadUserByUsername($credentials['username']);
        } catch (UsernameNotFoundException $e) {
            throw new CustomUserMessageAuthenticationException($this->unauthorized);
        }
    }

    protected function getLoginUrl() 
    {
        return $this->urlGenerator->generate('security_login');
    }
    
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse($this->urlGenerator->generate('homepage'));
    }
    
    public function supports(Request $request) 
    {
        if ($request->getPathInfo() != '/login_check' || !$request->isMethod('POST')) {
            return false;
        }
        
        return true;
    }
}
