<?php

namespace App\Security\User;

use App\Model\ApplicationUser;
use App\Model\ApplicationUserQuery;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class AppUserProvider implements UserProviderInterface
{
    public function loadUserByUsername($username) 
    {
        $user = ApplicationUserQuery::create()->setIgnoreCase(true)->findOneByUsername($username);
        
        if (null === $user) {
            throw new UsernameNotFoundException(sprintf('User "%s" does not exist.', $username));
        }
        
        return $user;
    }

    public function refreshUser(UserInterface $user) 
    {
        if (!$user instanceof ApplicationUser) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }
        
        return ApplicationUserQuery::create()->findPk($user->getId());
    }

    public function supportsClass($class) 
    {
        return $class === ApplicationUser::class;
    }
}