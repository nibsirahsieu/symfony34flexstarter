<?php

namespace App\Form;

use App\Model\ChangePassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder->add('current', PasswordType::class, array(
            'label' => 'app.label.current_password',
            'required' => true,
            'constraints' => [new NotBlank()]
        ));
        $builder->add('new', RepeatedType::class, array(
            'type'          => PasswordType::class,
            'first_options'  => [
                'label' => 'app.label.new_password',
                'required' => true,
                'constraints' => [new NotBlank(), new Length(['max' => 50])]
            ],
            'second_options' => ['label' => 'app.label.repeat_new_password'],
        ));
    }
    
    public function configureOptions(OptionsResolver $resolver) 
    {
        $resolver->setDefaults([
            'data_class' => ChangePassword::class,
            'csrf_token_id' => $this->getBlockPrefix()
        ]);
    }
}